from flask import Flask

app = Flask(__name__)

@app.route
def wish():
    message = "happy birthday {name}"
    return message.format(name=os.getenv("NAME", "John"))

if name == "__main__":
    app.run(host="0.0.0.0", port=8080)
